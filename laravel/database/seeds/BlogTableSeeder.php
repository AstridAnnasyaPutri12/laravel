<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog')->insert([
            'title' => 'Happy Independence Day! 10 Ucapan Hari Kemerdekaan Indonesia HUT RI ke-75',
            'content' => 'Jakarta - HUT RI ke-75 ini diselenggarakan pada 17 Agustus 2020. Peringatan proklamasi kemerdekaan Ini menjadi salah satu hari perayaan yang sangat menggembirakan untuk seluruh masyarakat Indonesia.',
            'category' => 'HUT RI Ke-75'
        ]);
    }
}
